
# shell script for checking vultr account balance.
Tested on FreeBSD-11 should also work on most \*nix.

Depends: curl jq awk bc (working mail)

Login to your vultr.com to enable/get Your API Key,
Account -> API -> Personal Access Token

#### Add this key to the script.

#### Change the email address.

#### Set the min\_credit amount.


\# chmod 0755 vultr\_balance\_check.sh

Example crontab

xx xx \* \* \* /bin/sh /full/path/to/vultr\_balance\_check.sh \>/dev/null 2>&1

## Added a python version

vultr\_balance\_check.py
