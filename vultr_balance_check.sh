#!/bin/sh

 
############################################

mykey='Your-API-Key'

# multiple email addresses separated by comma,
email='me@example.com'

# needs the hyphen
min_credit=-10

############################################


balance=$(curl -H "API-Key: ${mykey}" \
    https://api.vultr.com/v1/account/info \
    | jq -r '.balance') \
    >/dev/null 2>&1


# Remove leading '-' for display only
rbal="$(echo ${balance} | awk '{print substr($0,2)}')"


if [ 1 -eq "$(echo "${min_credit} < ${balance}" | bc)" ]
then  
    echo 'Please add funds to Vultr account, remaining balance: $'${rbal} \
    | mail -s "Vultr Account Low Credit Warning" ${email}

fi
