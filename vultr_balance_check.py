#!/usr/bin/env python
# -*- coding: UTF-8 -*-

"""Gets the balance from your vultr.com account
then sends email if below the trigger amount
Depends: python27, requests
eg.. FreeBSD-11: pkg install py27-requests

Use the key from your vultr.com account
Login -> Account -> API -> Your_API_Key

email settings can be changed in the send_email
function.

"""

###############################################
###############################################

API_KEY = 'Your_API_Key'
TRIGGER_AMOUNT = 10

USER_EMAIL = 'me@example.com'
USER_PWD = 'me-pwd'
RECIPIENT_EMAIL = 'recipient@example.com'

###############################################
###############################################

def get_balance(key, trigger_value):

    """returns the balance"""

    import requests

    try:
        url = 'https://api.vultr.com/v1/account/info'

        headers = {'API-Key': key}

        res = requests.get(url, headers=headers)

        if res.status_code != 200:
            print('Status:', res.status_code, 'Problem with request. Exiting.')
            exit()

        # remove leading - minus sign [1:]
        balance = float(res.json()['balance'][1:])

        if balance < trigger_value:
            return balance

    except (ValueError, TypeError, ZeroDivisionError, RuntimeError):
        print 'Oops something went wrong in get_balance'



BAL = get_balance(API_KEY, TRIGGER_AMOUNT)


def send_email(user, pwd, recipient, body):

    """Change these to suit your email settings"""

    #############################
    #############################

    smtp_host = 'smtp.example.com'
    smtp_port = 587
    subject = 'Vultr low on funds'

    #############################
    #############################

    import smtplib

    gmail_user = user
    gmail_pwd = pwd
    from_user = user
    to_rec = recipient
    subject = subject
    text = body

    # Prepare message
    message = """From: %s\nTo: %s\nSubject: %s\n\n%s
    """ % (from_user, to_rec, subject, text)
    try:
        server = smtplib.SMTP(smtp_host, smtp_port)
        server.ehlo()
        server.starttls()
        server.login(gmail_user, gmail_pwd)
        server.sendmail(from_user, to_rec, message)
        server.close()
        print 'Successfully sent the mail'
    except RuntimeError:
        print "Failed to send mail"


if BAL:
    BODY = 'Remaining balance: ${0}'.format(BAL)
    send_email(USER_EMAIL, USER_PWD, RECIPIENT_EMAIL, BODY)
